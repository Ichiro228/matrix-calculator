#ранг матриц
import numpy as np

def rank_find(matrix):
    try:
        a = str(matrix)
        a = np.array(np.mat(a))
        a = np.linalg.matrix_rank(a)
        a = str(a)
        #return print(a)
        return a
    except Exception:
        return "Ошибка ввода"


#rank_find('2,  2,  3; 4,  5,  6; 7,  8, 9', '2,  2,  3; 4,  5,  6; 7,  8, 9')