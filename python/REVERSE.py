#обратная матрица
import numpy as np

def reverse_find(matrix):
    try:
        a = str(matrix)
        a = np.array(np.mat(a))
        b = np.around(np.linalg.det(a), decimals=3)
        if b == 0.0:
            b = str(b)
            b = "Определитель равен 0. Система имеет бесконечное множество решений."
            return b
        b = np.around(np.linalg.inv(a), decimals=3)
        #a = str(a)
        b = str(b)
        #return print(a), print(b)
        return b
    except Exception:
        return "Ошибка ввода"


#reverse_find('2,  2,  3; 4,  5,  6; 7,  8, 9')