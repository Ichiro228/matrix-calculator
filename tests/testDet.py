import unittest
from DET_TEST import det


class TestDet(unittest.TestCase):

  def setUp(self):
    self.det = det()

  def test_add(self):
    self.assertEqual(self.det.det_find('1, 2, 3; 4, 5, 6; 7, 8, 9'), '0.0')

  def test_add2(self):
    self.assertEqual(self.det.det_find('1, 2, 3; 4, 5, 6; 7, 8, 8'), '3.0')


if __name__ == "__main__":
  unittest.main()