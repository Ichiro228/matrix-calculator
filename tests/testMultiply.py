import unittest
from MULTIPLY_TEST import multiply


class TestMinus(unittest.TestCase):

  def setUp(self):
    self.multiply = multiply()

  def test_add(self):
    a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    b = 100
    self.assertEqual(self.multiply.multiply_find(a, b), '[[100. 200. 300. 400. 500. 600. 700. 800. 900.]]')

  def test_add(self):
    a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    b = 0.3
    self.assertEqual(self.multiply.multiply_find(a, b), '[[0.3 0.6 0.9 1.2 1.5 1.8 2.1 2.4 2.7]]')

if __name__ == "__main__":
  unittest.main()