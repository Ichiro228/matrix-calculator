import unittest
from MINUS_TEST import minus


class TestMinus(unittest.TestCase):

  def setUp(self):
    self.minus = minus()

  def test_add(self):
    a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    b = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    self.assertEqual(self.minus.minus_find(a, b), '[[0 0 0]\n [0 0 0]\n [0 0 0]]')

  def test_add(self):
    a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    b = [[0, 0, 0], [12, 35, 61], [7.11, 83, 91]]
    self.assertEqual(self.minus.minus_find(a, b), '[[  1.     2.     3.  ]\n [ -8.   -30.   -55.  ]\n [ -0.11 -75.   -82.  ]]')




if __name__ == "__main__":
  unittest.main()