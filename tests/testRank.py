import unittest
from RANK_TEST import rank


class TestDet(unittest.TestCase):

  def setUp(self):
    self.rank = rank()

  def test_add(self):
    self.assertEqual(self.rank.rank_find('1, 2, 3; 4, 5, 6; 7, 8, 9'), '2')

  def test_add2(self):
    self.assertEqual(self.rank.rank_find('0, 0, 0; 0, 0, 0; 0, 0, 0'), '0')

if __name__ == "__main__":
  unittest.main()