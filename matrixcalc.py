from flask import Flask, render_template, request, url_for, redirect
from python import DET, REVERSE, POWER, MULTIPLY, MULTIPLY_MATRIX, TRANSPOSE, PLUS, MINUS, RANK


app = Flask(__name__)



@app.route('/')
def index():
    return render_template('index.html')



@app.route('/about')
def about():
    return render_template('about.html')


@app.errorhandler(404)
def pageNotFound(error):
    return render_template('page404.html')


############################################################################
@app.route('/det')
def det():
    return render_template("det.html")


@app.route('/det_method', methods=['POST'])
def det_method():
    inputMatrix = request.form['matrix']
    inputMatrix = DET.det_find(inputMatrix)
    return render_template('det.html', matrix = inputMatrix)


##################################################################


@app.route('/reverse')
def reverse():
    return render_template("reverse.html")


@app.route('/reverse_method', methods=['POST'])
def reverse_method():
    inputMatrix = request.form['matrix']
    inputMatrix = REVERSE.reverse_find(inputMatrix)
    return render_template('reverse.html', matrix = inputMatrix)


##################################################################


@app.route('/power')
def power():
    return render_template("power.html")


@app.route('/power_method', methods=['POST'])
def power_method():
    inputMatrix = request.form['matrix']
    inputPower = request.form['power']
    inputMatrix = POWER.power_find(inputMatrix, inputPower)
    return render_template('power.html', matrix = inputMatrix)


##################################################################


@app.route('/multiply')
def multiply():
    return render_template("multiply.html")


@app.route('/multiply_method', methods=['POST'])
def multiply_method():
    inputMatrix = request.form['matrix']
    inputMultiply = request.form['multiply']
    inputMatrix = MULTIPLY.multiply_find(inputMatrix, inputMultiply)
    return render_template('multiply.html', matrix = inputMatrix)


##################################################################


@app.route('/multiply_matrix')
def multiply_matrix():
    return render_template("multiply_matrix.html")


@app.route('/multiply_matrix_method', methods=['POST'])
def multiply_matrix_method():
    inputMatrix = request.form['matrix']
    inputMatrix1 = request.form['matrix1']
    inputMatrix = MULTIPLY_MATRIX.multiply_matrix_find(inputMatrix, inputMatrix1)
    return render_template('multiply_matrix.html', matrix = inputMatrix)


##################################################################


@app.route('/transpose')
def transpose():
    return render_template("transpose.html")


@app.route('/transpose_method', methods=['POST'])
def transpose_method():
    inputMatrix = request.form['matrix']
    inputMatrix = TRANSPOSE.transpose_find(inputMatrix)
    return render_template('transpose.html', matrix = inputMatrix)


##################################################################


@app.route('/plus')
def plus():
    return render_template("plus.html")


@app.route('/plus_method', methods=['POST'])
def plus_method():
    inputMatrix = request.form['matrix']
    inputMatrix1 = request.form['matrix1']
    inputMatrix = PLUS.plus_find(inputMatrix, inputMatrix1)
    return render_template('plus.html', matrix = inputMatrix)


##################################################################


@app.route('/minus')
def minus():
    return render_template("minus.html")


@app.route('/minus_method', methods=['POST'])
def minus_method():
    inputMatrix = request.form['matrix']
    inputMatrix1 = request.form['matrix1']
    inputMatrix = MINUS.minus_find(inputMatrix, inputMatrix1)
    return render_template('minus.html', matrix = inputMatrix)


##################################################################


@app.route('/rank')
def rank():
    return render_template("rank.html")


@app.route('/rank_method', methods=['POST'])
def rank_method():
    inputMatrix = request.form['matrix']
    inputMatrix = RANK.rank_find(inputMatrix)
    return render_template('rank.html', matrix = inputMatrix)


##################################################################

if __name__ == "__main__":
    app.run(debug=False)